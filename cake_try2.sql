-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: cake_try1
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course_enrolments`
--

DROP TABLE IF EXISTS `course_enrolments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course_enrolments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned DEFAULT NULL,
  `course_id` bigint unsigned DEFAULT NULL,
  `enrolment_date` timestamp NULL DEFAULT NULL,
  `row_status` tinyint unsigned NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `course_enrolments_user_id_course_id_unique` (`user_id`,`course_id`),
  KEY `course_enrolments_course_id_foreign` (`course_id`),
  CONSTRAINT `course_enrolments_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `course_enrolments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_enrolments`
--

LOCK TABLES `course_enrolments` WRITE;
/*!40000 ALTER TABLE `course_enrolments` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_enrolments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `course_title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `course_description_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `overall_rating` decimal(3,2) unsigned NOT NULL DEFAULT '0.00',
  `test_count` int unsigned NOT NULL DEFAULT '1',
  `is_published` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` bigint unsigned DEFAULT NULL,
  `row_status` tinyint unsigned NOT NULL DEFAULT '1',
  `course_links` json DEFAULT NULL,
  `required_pre_assessment` tinyint(1) NOT NULL DEFAULT '0',
  `pre_assessment_passmark` smallint NOT NULL DEFAULT '0',
  `pass_mark` smallint NOT NULL DEFAULT '70',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_created_id_foreign` (`user_id`),
  CONSTRAINT `courses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (13,'A New Course','asdf asdf asdf  bangla','asdf asfd asdf asf tggdf English',0.00,1,1,2,1,'\"asdfasdf\"',0,0,70,NULL,'2022-10-06 06:08:37','2022-10-06 06:10:27'),(14,'A New Course2','this is new course 2','this is new course 2',0.00,1,1,2,1,'\"https://jqueryvalidation.org/files/demo/\"',0,0,70,NULL,'2022-10-10 04:30:51','2022-10-10 04:30:51');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_questions`
--

DROP TABLE IF EXISTS `exam_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_questions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `exam_id` bigint unsigned NOT NULL,
  `question_title` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_a` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_b` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_c` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_d` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` int unsigned NOT NULL,
  `row_status` tinyint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questions_exam_id_foreign` (`exam_id`),
  CONSTRAINT `exam_questions_test_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_questions`
--

LOCK TABLES `exam_questions` WRITE;
/*!40000 ALTER TABLE `exam_questions` DISABLE KEYS */;
INSERT INTO `exam_questions` VALUES (69,9,'question 1','one','two','three','four',1,1,'2022-10-09 06:25:28','2022-10-09 10:35:23'),(70,9,'question2','one','two','three','four',1,1,'2022-10-09 06:25:28','2022-10-09 10:35:23'),(73,9,'question 4','one','two','three','four',1,1,'2022-10-09 11:31:12','2022-10-09 11:31:12'),(74,9,'question 5','one','two','three','four',1,1,'2022-10-09 11:31:12','2022-10-09 11:31:12'),(75,9,'replacement for 3','one','two','three','four',1,1,'2022-10-09 11:32:49','2022-10-09 11:32:49'),(76,9,'A new one','one','two','three','four',1,1,'2022-10-09 11:32:49','2022-10-09 11:32:49'),(77,10,'question2','one','two','three','four',1,1,'2022-10-10 04:40:53','2022-10-10 04:40:53'),(78,10,'question 3','one','two','three','four',1,1,'2022-10-10 04:40:53','2022-10-10 04:40:53'),(79,10,'question 5','one','two','three','four',1,1,'2022-10-10 04:40:53','2022-10-10 04:40:53'),(80,10,'question 6','one','two','three','four',1,1,'2022-10-10 04:40:53','2022-10-10 04:40:53'),(81,11,'question 1','one','two','three','four',1,1,'2022-10-10 05:29:43','2022-10-10 05:29:43'),(82,11,'question2','one','two','three','four',1,1,'2022-10-10 05:29:43','2022-10-10 05:29:43'),(83,11,'question 3','one','two','three','four',1,1,'2022-10-10 05:29:43','2022-10-10 05:29:43'),(84,11,'question 4','one','two','three','four',1,1,'2022-10-10 05:29:43','2022-10-10 05:29:43'),(85,11,'question 5','one','two','three','four',1,1,'2022-10-10 05:29:43','2022-10-10 05:29:43'),(86,12,'question 1','one','two','three','four',1,1,'2022-10-10 05:31:55','2022-10-10 05:31:55'),(87,12,'question2','one','two','three','four',1,1,'2022-10-10 05:31:55','2022-10-10 05:31:55'),(88,12,'question 3','one','two','three','four',1,1,'2022-10-10 05:31:55','2022-10-10 05:31:55'),(89,12,'question 4','one','two','three','four',1,1,'2022-10-10 05:31:55','2022-10-10 05:31:55');
/*!40000 ALTER TABLE `exam_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exams`
--

DROP TABLE IF EXISTS `exams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exams` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by` bigint unsigned DEFAULT NULL,
  `course_id` bigint unsigned DEFAULT NULL,
  `title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pass_marks` int unsigned NOT NULL DEFAULT '50',
  `duration` smallint unsigned NOT NULL,
  `total_marks` smallint unsigned NOT NULL,
  `test_type` smallint unsigned NOT NULL DEFAULT '2',
  `row_status` tinyint unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exams_created_by_foreign` (`created_by`),
  KEY `exams_course_by_foreign` (`course_id`),
  CONSTRAINT `exam_course_by_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `exams_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exams`
--

LOCK TABLES `exams` WRITE;
/*!40000 ALTER TABLE `exams` DISABLE KEYS */;
INSERT INTO `exams` VALUES (9,NULL,13,'test question','test exams',50,20,50,2,0,NULL,NULL),(10,NULL,14,'Text Exam','test exam for course 2',50,20,50,2,0,NULL,NULL),(11,NULL,13,'Test2_1','aother test for course1',50,20,50,2,0,NULL,NULL),(12,2,14,'2nd test','aother test for course2',50,20,50,2,0,NULL,NULL);
/*!40000 ALTER TABLE `exams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phinxlog`
--

DROP TABLE IF EXISTS `phinxlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `phinxlog` (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phinxlog`
--

LOCK TABLES `phinxlog` WRITE;
/*!40000 ALTER TABLE `phinxlog` DISABLE KEYS */;
INSERT INTO `phinxlog` VALUES (20221027122110,'CreateTags','2022-10-27 06:21:29','2022-10-27 06:21:29',0);
/*!40000 ALTER TABLE `phinxlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT 'null',
  `image` varchar(150) DEFAULT 'null',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (5,'aminul','asdfafs','uploads/1666866347aminulImage.jpeg'),(4,'aminul','asdfasdf','uploads/aminulImage.jpeg1666865802');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'general','2022-10-04 11:27:00','2022-10-04 11:27:00'),(2,'admin','2022-10-04 11:28:00','2022-10-04 11:28:00');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tags` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_details` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `phone` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_details`
--

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` VALUES (1,2,'aminul','012457896','2022-10-04 11:31:00','2022-10-05 08:12:30'),(2,7,'aminulNo4','01704762867',NULL,'2022-10-05 05:54:44'),(3,8,'aminulNo5','01704762867',NULL,NULL),(4,9,'aminulNo6','01704762867',NULL,NULL),(5,10,'aminulNo7','01704762867','2022-10-05 05:34:33','2022-10-05 05:54:17'),(6,11,'aminulNo8','01704762867','2022-10-05 05:37:03','2022-10-05 05:37:03'),(7,3,'AminulTheSecond','01704562867','2022-10-05 08:11:03','2022-10-05 08:11:03'),(8,12,'User General','01704762867','2022-10-05 10:15:53','2022-10-05 10:15:53');
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint DEFAULT NULL,
  `user_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_users` (`user_name`),
  UNIQUE KEY `email_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,2,'aminul','aminul@aminul.com','$2y$10$bqgmNgfF1eABVEG38NnsBOqdQZQo5fzyrXLYHbYm.oDWIctHp8x0e','2022-10-04 12:16:00','2022-10-05 10:30:31'),(3,1,'aminul_2','aminul2@aminul.com','$2y$10$zlFwsXJ3iQErwM.CLvcflOIP3qMRIpYsKBC18/e4yV0ICiyAtJDNm',NULL,'2022-10-05 10:08:22'),(7,NULL,'aminul4','aminul4@aminul.com','$2y$10$Gu9WPdn9AU6bIZzr7RTokuAC42werOpDBtY6R17uof/GXGZDd26xe',NULL,'2022-10-05 08:27:44'),(8,2,'aminul5','aminul5@aminul.com','$2y$10$EINWfKiAgFFZiNg7SS5mAuPjJTouBvbeP28hQjNWfMTRfm4ZRWeoW',NULL,NULL),(9,1,'aminul6','aminul6@aminul.com','$2y$10$nG4LCJ1/SZfT5TXKPp0iReVmu0b1osuMz9HcU3p1.C9cp1FOf1WiK',NULL,NULL),(10,1,'aminul7','aminul7@aminul.com','$2y$10$1A3oIWJqcT/hy0ExIFTrkecSZ.iHi4prtoh4QHVnS.xh6gHH3weUC','2022-10-05 05:34:33',NULL),(11,1,'aminul8','aminul8@aminul.com','$2y$10$tu31U6VtLzXXOXCo9lxFtez35abcrZvaGV6xke/6RgjySwlLApQkS','2022-10-05 05:37:03','2022-10-05 05:37:03'),(12,1,'generalUser','general@aminul.com','$2y$10$QLFOfVchsbVCFM/QrmzUDe1s95SsS.ZuGnY3HmcJcFakn.F8AYgIq','2022-10-05 10:15:53','2022-10-05 10:15:53');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-27 18:46:47
