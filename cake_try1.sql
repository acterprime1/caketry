-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 06, 2022 at 12:12 PM
-- Server version: 8.0.27
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cake_try1`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
CREATE TABLE IF NOT EXISTS `courses` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `course_title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `course_description_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `overall_rating` decimal(3,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `test_count` int UNSIGNED NOT NULL DEFAULT '1',
  `is_published` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `row_status` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `course_links` json DEFAULT NULL,
  `required_pre_assessment` tinyint(1) NOT NULL DEFAULT '0',
  `pre_assessment_passmark` smallint NOT NULL DEFAULT '0',
  `pass_mark` smallint NOT NULL DEFAULT '70',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_created_id_foreign` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_title`, `course_description`, `course_description_en`, `overall_rating`, `test_count`, `is_published`, `user_id`, `row_status`, `course_links`, `required_pre_assessment`, `pre_assessment_passmark`, `pass_mark`, `deleted_at`, `created_at`, `updated_at`) VALUES
(13, 'A New Course', 'asdf asdf asdf  bangla', 'asdf asfd asdf asf tggdf English', '0.00', 1, 1, 2, 1, '\"asdfasdf\"', 0, 0, 70, NULL, '2022-10-06 06:08:37', '2022-10-06 06:10:27');

-- --------------------------------------------------------

--
-- Table structure for table `course_enrolments`
--

DROP TABLE IF EXISTS `course_enrolments`;
CREATE TABLE IF NOT EXISTS `course_enrolments` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `course_id` bigint UNSIGNED DEFAULT NULL,
  `enrolment_date` timestamp NULL DEFAULT NULL,
  `row_status` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `course_enrolments_user_id_course_id_unique` (`user_id`,`course_id`),
  KEY `course_enrolments_course_id_foreign` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

DROP TABLE IF EXISTS `exams`;
CREATE TABLE IF NOT EXISTS `exams` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_by` bigint UNSIGNED DEFAULT NULL,
  `course_id` bigint UNSIGNED DEFAULT NULL,
  `title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pass_marks` int UNSIGNED NOT NULL DEFAULT '50',
  `duration` smallint UNSIGNED NOT NULL,
  `total_marks` smallint UNSIGNED NOT NULL,
  `test_type` smallint UNSIGNED NOT NULL DEFAULT '2',
  `row_status` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exams_created_by_foreign` (`created_by`),
  KEY `exams_course_by_foreign` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `exam_questions`
--

DROP TABLE IF EXISTS `exam_questions`;
CREATE TABLE IF NOT EXISTS `exam_questions` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `exam_id` bigint UNSIGNED NOT NULL,
  `question_title` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_a` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_b` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_c` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_d` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` int UNSIGNED NOT NULL,
  `row_status` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questions_exam_id_foreign` (`exam_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'general', '2022-10-04 11:27:00', '2022-10-04 11:27:00'),
(2, 'admin', '2022-10-04 11:28:00', '2022-10-04 11:28:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` bigint DEFAULT NULL,
  `user_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_users` (`user_name`),
  UNIQUE KEY `email_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `user_name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(2, 2, 'aminul', 'aminul@aminul.com', '$2y$10$bqgmNgfF1eABVEG38NnsBOqdQZQo5fzyrXLYHbYm.oDWIctHp8x0e', '2022-10-04 12:16:00', '2022-10-05 10:30:31'),
(3, 1, 'aminul_2', 'aminul2@aminul.com', '$2y$10$zlFwsXJ3iQErwM.CLvcflOIP3qMRIpYsKBC18/e4yV0ICiyAtJDNm', NULL, '2022-10-05 10:08:22'),
(7, NULL, 'aminul4', 'aminul4@aminul.com', '$2y$10$Gu9WPdn9AU6bIZzr7RTokuAC42werOpDBtY6R17uof/GXGZDd26xe', NULL, '2022-10-05 08:27:44'),
(8, 2, 'aminul5', 'aminul5@aminul.com', '$2y$10$EINWfKiAgFFZiNg7SS5mAuPjJTouBvbeP28hQjNWfMTRfm4ZRWeoW', NULL, NULL),
(9, 1, 'aminul6', 'aminul6@aminul.com', '$2y$10$nG4LCJ1/SZfT5TXKPp0iReVmu0b1osuMz9HcU3p1.C9cp1FOf1WiK', NULL, NULL),
(10, 1, 'aminul7', 'aminul7@aminul.com', '$2y$10$1A3oIWJqcT/hy0ExIFTrkecSZ.iHi4prtoh4QHVnS.xh6gHH3weUC', '2022-10-05 05:34:33', NULL),
(11, 1, 'aminul8', 'aminul8@aminul.com', '$2y$10$tu31U6VtLzXXOXCo9lxFtez35abcrZvaGV6xke/6RgjySwlLApQkS', '2022-10-05 05:37:03', '2022-10-05 05:37:03'),
(12, 1, 'generalUser', 'general@aminul.com', '$2y$10$QLFOfVchsbVCFM/QrmzUDe1s95SsS.ZuGnY3HmcJcFakn.F8AYgIq', '2022-10-05 10:15:53', '2022-10-05 10:15:53');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
CREATE TABLE IF NOT EXISTS `user_details` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `phone` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `name`, `phone`, `created_at`, `updated_at`) VALUES
(1, 2, 'aminul', '012457896', '2022-10-04 11:31:00', '2022-10-05 08:12:30'),
(2, 7, 'aminulNo4', '01704762867', NULL, '2022-10-05 05:54:44'),
(3, 8, 'aminulNo5', '01704762867', NULL, NULL),
(4, 9, 'aminulNo6', '01704762867', NULL, NULL),
(5, 10, 'aminulNo7', '01704762867', '2022-10-05 05:34:33', '2022-10-05 05:54:17'),
(6, 11, 'aminulNo8', '01704762867', '2022-10-05 05:37:03', '2022-10-05 05:37:03'),
(7, 3, 'AminulTheSecond', '01704562867', '2022-10-05 08:11:03', '2022-10-05 08:11:03'),
(8, 12, 'User General', '01704762867', '2022-10-05 10:15:53', '2022-10-05 10:15:53');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `course_enrolments`
--
ALTER TABLE `course_enrolments`
  ADD CONSTRAINT `course_enrolments_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `course_enrolments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `exams`
--
ALTER TABLE `exams`
  ADD CONSTRAINT `exam_course_by_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `exams_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `exam_questions`
--
ALTER TABLE `exam_questions`
  ADD CONSTRAINT `exam_questions_test_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
