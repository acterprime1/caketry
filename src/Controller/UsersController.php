<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\Query;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        // Add the 'add' action to the allowed actions list.
        $this->Auth->allow(['register']);

    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if(!$this->isAdmin())
        {
            $this->Flash->error(__('You are not allowed to visit this page'));
            return $this->redirect('/dashboard');
        }
        $this->paginate = [
            'contain' => ['Roles']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles', 'UserDetails']
        ]);


        $slug = $user->slug('My article name');
        dd(slug);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function dashboard()
    {

        $user = $user = $this->Users->get($this->Auth->user()['id'], [
            'contain' => ['Roles', 'UserDetails']
        ]);
       // dd($user->role);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if(!$this->isAdmin())
        {
            $this->Flash->error(__('You are not allowed to visit this page'));
            return $this->redirect('/dashboard');
        }
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData(), [
                'associated' => [
                    'UserDetails'
                ]
            ]);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles'));
        $this->set('_serialize', ['user']);
    }

    public function register()
    {
        if($this->Auth->user()){
           return $this->redirect(['action' => 'dashboard']);
        }
        $user = $this->Users->newEntity();
        $patchedData = $this->request->getData();
        $patchedData['role_id'] = $this->getGeneralRole();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $patchedData,[
                'associated' => [
                'UserDetails'
            ]
            ]);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['controller'=>'login', 'action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        //$user = $this->Users->get($id);
        if(!($this->isAdmin() || $this->Auth->user()['id'] == $id))
        {

            $this->Flash->error(__('You are not allowed to visit this page'));
            return $this->redirect('/dashboard');
        }
       try{

          /*
           $user = $this->Users->find('all')
               ->contain('UserDetails')
               ->where(['users.id' => $id])
               ->firstOrFail();
          */

             $user = $this->Users
            ->findById($id)
            ->contain(['UserDetails'], true)
           ->firstOrFail();

        }
       catch (\Exception $e)
       {

           $this->Flash->error($e->getMessage());
           return $this->redirect('/dashboard');
       }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData(), [
                'associated' => [
                    'UserDetails'
                ]
            ]);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'dashboard']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if(!$this->isAdmin())
        {
            $this->Flash->error(__('You are not allowed to visit this page'));
            return $this->redirect('/dashboard');
        }
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
