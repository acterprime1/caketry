<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Collection\Collection;

/**
 * Exams Controller
 *
 * @property \App\Model\Table\ExamsTable $Exams
 *
 * @method \App\Model\Entity\Exam[] paginate($object = null, array $settings = [])
 */
class ExamsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Courses']
        ];
        $exams = $this->paginate($this->Exams);

        $this->set(compact('exams'));
        $this->set('_serialize', ['exams']);
    }

    /**
     * View method
     *
     * @param string|null $id Exam id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $exam = $this->Exams->get($id, [
            'contain' => ['Courses', 'ExamQuestions', 'Users.UserDetails']
        ]);
        $this->viewBuilder()->setLayout('bootstrapadmin');
        $this->set('exam', $exam);
        $this->set('_serialize', ['exam']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $exam = $this->Exams->newEntity();
        $this->viewBuilder()->setLayout('bootstrapadmin');
        if ($this->request->is('post')) {

            $patched_data = $this->request->getData();
            dd($patched_data);
            $patched_data['user_id'] = $this->Auth->user()['id'];
            $patched_data['created_by'] = $this->Auth->user()['id'];
            try {
                $exam = $this->Exams->patchEntity($exam, $patched_data,
                    [
                        'associated' => [
                            'ExamQuestions'
                            ]
                    ]

                );

               if ($this->Exams->save($exam)) {
                    $this->Flash->success(__('The exam has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }
               else{
                   dd($exam->getErrors() );
               }


            }
            catch (\Exception $e){
                $this->Flash->error(__($e->getMessage()));
            }

        }
        $courses = $this->Exams->Courses->find('list', ['limit' => 200]);
        $courseList  = $this->Exams->Courses->find()->select(['id', 'course_title']);
        $this->set(compact('exam', 'courses', 'courseList'));
        $this->set('_serialize', ['exam']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Exam id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $exam = $this->Exams->get($id, [
            'contain' => ['ExamQuestions']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $patched_data = $this->request->getData();

            //dd($this->request->getData());
            $exam = $this->Exams->patchEntity($exam, $patched_data,
                [
                    'associated' => [
                        'ExamQuestions'
                    ]
                ]

            );
            if ($this->Exams->save($exam)) {
                $this->Flash->success(__('The exam has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The exam could not be saved. Please, try again.'));
        }
        $max_question_id = (new Collection($exam->exam_questions))->max('id')->id;

        $courses = $this->Exams->Courses->find('list', ['limit' => 200]);
        $this->set(compact('exam', 'courses', 'max_question_id'));
        $this->set('_serialize', ['exam']);
        //dd((new Collection($exam->exam_questions))->max('id')->id);
        $this->viewBuilder()->setLayout('bootstrapadmin');
    }

    /**
     * Delete method
     *
     * @param string|null $id Exam id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $exam = $this->Exams->get($id);
        if ($this->Exams->delete($exam)) {
            $this->Flash->success(__('The exam has been deleted.'));
        } else {
            $this->Flash->error(__('The exam could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
