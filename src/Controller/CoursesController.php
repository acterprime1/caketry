<?php
namespace App\Controller;

use App\Controller\AppController;
use mysql_xdevapi\Exception;

/**
 * Courses Controller
 *
 * @property \App\Model\Table\CoursesTable $Courses
 *
 * @method \App\Model\Entity\Course[] paginate($object = null, array $settings = [])
 */
class CoursesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if(!($this->isAdmin() || $this->Auth->user()['id'] == $id))
        {

            $this->Flash->error(__('You are not allowed to visit this page'));
            return $this->redirect('/dashboard');
        }
        $this->paginate = [
            'contain' => ['Users']
        ];
        $courses = $this->paginate($this->Courses);

        $this->set(compact('courses'));
        $this->set('_serialize', ['courses']);
    }

    /**
     * View method
     *
     * @param string|null $id Course id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if(!($this->isAdmin() || $this->Auth->user()['id'] == $id))
        {

            $this->Flash->error(__('You are not allowed to visit this page'));
            return $this->redirect('/dashboard');
        }

        $course = $this->Courses->get($id, [
            'contain' => ['Users', 'CourseEnrolments', 'Exams']
        ]);

        $this->set('course', $course);
        $this->set('_serialize', ['course']);
    }

    public function generalView($id = null)
    {
       $course = $this->Courses->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('course', $course);
        $this->set('_serialize', ['course']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if(!($this->isAdmin() || $this->Auth->user()['id'] == $id))
        {

            $this->Flash->error(__('You are not allowed to visit this page'));
            return $this->redirect('/dashboard');
        }
        $course = $this->Courses->newEntity();
        if ($this->request->is('post')) {
            $patched_data = $this->request->getData();
            $patched_data['user_id'] = $this->Auth->user()['id'];
            try {


                $course = $this->Courses->patchEntity($course, $patched_data);
                 $this->Courses->save($course);
                 $this->Flash->success(__('The course has been saved.'));
                 return $this->redirect(['action' => 'index']);
            }
            catch (\Exception $exception){
                $this->Flash->error(__($exception->getMessage()));
            }

        }
        $users = $this->Courses->Users->find('list', ['limit' => 200]);
        $this->set(compact('course', 'users'));
        $this->set('_serialize', ['course']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Course id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if(!($this->isAdmin() || $this->Auth->user()['id'] == $id))
        {

            $this->Flash->error(__('You are not allowed to visit this page'));
            return $this->redirect('/dashboard');
        }

        $course = $this->Courses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $course = $this->Courses->patchEntity($course, $this->request->getData());
            if ($this->Courses->save($course)) {
                $this->Flash->success(__('The course has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The course could not be saved. Please, try again.'));
        }
        $users = $this->Courses->Users->find('list', ['limit' => 200]);
        $this->set(compact('course', 'users'));
        $this->set('_serialize', ['course']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Course id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if(!($this->isAdmin() || $this->Auth->user()['id'] == $id))
        {

            $this->Flash->error(__('You are not allowed to visit this page'));
            return $this->redirect('/dashboard');
        }
        $this->request->allowMethod(['post', 'delete']);
        $course = $this->Courses->get($id);
        if ($this->Courses->delete($course)) {
            $this->Flash->success(__('The course has been deleted.'));
        } else {
            $this->Flash->error(__('The course could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
