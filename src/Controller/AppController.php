<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Login',
                'action' => 'index'
            ],
            'loginRedirect' => ['_name' => 'Dashboard'],
            'logoutRedirect' => [
                'controller' => 'Login',
                'action' => 'index'
            ],
            // If unauthorized, return them to page they were just on
            'unauthorizedRedirect' => $this->referer()

        ]);

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
        $auth = $this->Auth->user();
        $this->set('auth', $auth);
        $this->setLayout();

    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function isAuthorized($user, $role){
        if(!$user->role)
        {
            return false;
        }

       return $user->role_id == $role;

    }

    public function isAdmin()
    {
        return $this->Auth->user()['role_id'] == 2;
    }

    protected function getGeneralRole(){
        $roles = TableRegistry::get('Roles');
        return $roles->find()
            ->where(['name' => 'general'])
            ->first()->id;

    }
    protected function setLayout()
    {
        if(!$this->Auth->user()){
            $this->viewBuilder()->setLayout('guest');
            return;
        }
        $layout = $this->Auth->user()['role_id']== 2 ? 'admin': 'general';
        $this->viewBuilder()->setLayout($layout);

    }

}
