<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Course Entity
 *
 * @property int $id
 * @property string $course_title
 * @property string $course_description
 * @property string $course_description_en
 * @property float $overall_rating
 * @property int $test_count
 * @property bool $is_published
 * @property int $user_id
 * @property int $row_status
 * @property $course_links
 * @property bool $required_pre_assessment
 * @property int $pre_assessment_passmark
 * @property int $pass_mark
 * @property \Cake\I18n\FrozenTime $deleted_at
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $updated_at
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\CourseEnrolment[] $course_enrolments
 * @property \App\Model\Entity\Exam[] $exams
 */
class Course extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
