<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Courses Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CourseEnrolmentsTable|\Cake\ORM\Association\HasMany $CourseEnrolments
 * @property \App\Model\Table\ExamsTable|\Cake\ORM\Association\HasMany $Exams
 *
 * @method \App\Model\Entity\Course get($primaryKey, $options = [])
 * @method \App\Model\Entity\Course newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Course[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Course|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Course patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Course[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Course findOrCreate($search, callable $callback = null, $options = [])
 */
class CoursesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('courses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'updated_at' => 'always'
                ]
            ]
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CourseEnrolments', [
            'foreignKey' => 'course_id'
        ]);
        $this->hasMany('Exams', [
            'foreignKey' => 'course_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('course_title');

        $validator
            ->allowEmpty('course_description');

        $validator
            ->allowEmpty('course_description_en');

        $validator
            ->decimal('overall_rating')
            ->allowEmpty('overall_rating', 'create')
            ->allowEmpty('overall_rating');

        $validator
            ->integer('test_count')
            ->allowEmpty('test_count', 'create')
            ->allowEmpty('test_count');

        $validator
            ->boolean('is_published')
            ->allowEmpty('is_published', 'create')
            ->allowEmpty('is_published');

        $validator
            ->integer('row_status')
            ->allowEmpty('row_status', 'create')
            ->allowEmpty('row_status');

        $validator
            ->allowEmpty('course_links');

        $validator
            ->boolean('required_pre_assessment')
            ->allowEmpty('required_pre_assessment', 'create')
            ->allowEmpty('required_pre_assessment');

        $validator
            ->integer('pre_assessment_passmark')
            ->allowEmpty('pre_assessment_passmark', 'create')
            ->allowEmpty('pre_assessment_passmark');

        $validator
            ->integer('pass_mark')
            ->requirePresence('pass_mark', 'create')
            ->notEmpty('pass_mark');

        $validator
            ->dateTime('deleted_at')
            ->allowEmpty('deleted_at');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
