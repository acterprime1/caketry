<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ExamQuestions Model
 *
 * @property \App\Model\Table\ExamsTable|\Cake\ORM\Association\BelongsTo $Exams
 *
 * @method \App\Model\Entity\ExamQuestion get($primaryKey, $options = [])
 * @method \App\Model\Entity\ExamQuestion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ExamQuestion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ExamQuestion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ExamQuestion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ExamQuestion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ExamQuestion findOrCreate($search, callable $callback = null, $options = [])
 */
class ExamQuestionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('exam_questions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'updated_at' => 'always'
                ]
            ]
        ]);

        $this->belongsTo('Exams', [
            'foreignKey' => 'exam_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('question_title', 'create')
            ->notEmpty('question_title');

        $validator
            ->requirePresence('option_a', 'create')
            ->notEmpty('option_a');

        $validator
            ->requirePresence('option_b', 'create')
            ->notEmpty('option_b');

        $validator
            ->requirePresence('option_c', 'create')
            ->allowEmpty('option_c');

        $validator
            ->requirePresence('option_d', 'create')
            ->allowEmpty('option_d');

        $validator
            ->integer('answer')
            ->requirePresence('answer', 'create')
            ->notEmpty('answer');

        $validator
            ->integer('row_status')
            ->allowEmpty('row_status', 'create')
            ->allowEmpty('row_status');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['exam_id'], 'Exams'));

        return $rules;
    }
}
