<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Profile'), '/dashboard' ) ?> </li>
        <li><?= $this->Html->link(__('Edit Profile'), ['action' => 'edit', $user->id]) ?></li>

    </ul>
</nav>
