<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Exam $exam
  */
?>
<div class="exams view large-9 medium-8 columns content">
    <h3><?= h($exam->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Course') ?></th>
            <td><?= $exam->has('course') ? $this->Html->link($exam->course->id, ['controller' => 'Courses', 'action' => 'view', $exam->course->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($exam->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($exam->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($exam->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created By') ?></th>
            <td><?= h($exam->user ? $exam->user->user_detail->name : '' ) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pass Marks') ?></th>
            <td><?= $this->Number->format($exam->pass_marks) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Duration') ?></th>
            <td><?= $this->Number->format($exam->duration) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Marks') ?></th>
            <td><?= $this->Number->format($exam->total_marks) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Test Type') ?></th>
            <td><?= $this->Number->format($exam->test_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Row Status') ?></th>
            <td><?= $this->Number->format($exam->row_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($exam->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($exam->updated_at) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Exam Questions') ?></h4>

        <?php if (!empty($exam->exam_questions)): ?>
            <div id="question-container">
            <?php foreach ($exam->exam_questions as $examQuestions): ?>

                <div class="question-div">
                    <h1>Question no </h1>
                    <div class="d-flex flex-wrap pb-3">
                        <div class="form-group input select col-sm-6 col-md-4 d-flex flex-column">

                            <label for="pwd">Write the Question</label>
                            <input type="text" class="form-control" disabled placeholder=""  name="exam_questions[0][question_title]" value="<?= h($examQuestions->question_title) ?>">


                        </div>

                        <div class="form-group col-sm-6 col-md-4">
                            <label for="pwd">Correct Answer</label>
                            <?= $this->Form->select('exam_questions[0][answer]',
                             [1 => "Option A", 2 =>"Option B", 3 => "Option C", 4 => "Option D"],
                            [
                                'value' => $examQuestions->answer,
                                'disabled' => true,
                                'class' => 'selectpicker'
                            ]

                            );?>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap">
                        <div class="form-group col-sm-6 col-md-4">
                            <label for="pwd">Answer Option A</label>
                            <input type="text" class="form-control" disabled placeholder=""   name="exam_questions[0][option_a]" value="<?= h($examQuestions->option_a) ?>">
                        </div>


                        <div class="form-group col-sm-6 col-md-4">
                            <label for="pwd">Answer Option B</label>
                            <input type="text" class="form-control" disabled placeholder=""   name="exam_questions[0][option_b]" value="<?= h($examQuestions->option_b) ?>">
                        </div>


                        <div class="form-group col-sm-6 col-md-4">
                            <label for="pwd">Answer Option C</label>
                            <input type="text" class="form-control" disabled placeholder=""   name="exam_questions[0][option_c]" value="<?= h($examQuestions->option_c) ?>">
                        </div>


                        <div class="form-group col-sm-6 col-md-4">
                            <label for="pwd">Answer Option D</label>
                            <input type="text" class="form-control" disabled placeholder=""   name="exam_questions[0][option_d]" value="<?= h($examQuestions->option_d) ?>">
                        </div>

                    </div>


                </div>
            <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<style>
    .page-content{
        width: 100%;
        display: flex;
        flex-direction: column;
    }
    .page-header{
        display: flex;
        justify-content: space-between;
        margin-bottom: 10px;
        margin-top: 10px;
    }
    .form-content
    {
        display: flex;
        flex-direction: column;
        width: 100%;
    }
    /*.selectpicker{*/

    /*    width: 100%;*/
    /*    height: calc(1.5em + 0.75rem + 2px);*/
    /* }*/
    .selectpicker {
        width: 100%;
        height: calc(1.5em + 0.75rem + 2px);
        background: white;
        border: 1px solid lightgray;

        border-radius: 5px;
    }
    .selectpicker:active{
        border: 3px solid #1585fe;

    }
    .primary{
        background: #0095C8;
        color: white;
    }
    .label-button{
        font-size: 12px;
        padding: 6px;
        background: white;
        color: #007bff;
        border: 1px solid;
        border-radius: 5px;
    }
    .label-button:hover{
        color: white;
        background: #007bff;

    }
    #question-container {
        counter-reset: question;
    }
    h1::after {
        counter-increment: question;
        content:  " " counter(question) ;
    }
</style>
