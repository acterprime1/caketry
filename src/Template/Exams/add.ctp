<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="exams form large-9 medium-8 columns content">
    <?= $this->Form->create($exam) ?>
    <fieldset>
        <legend><?= __('Add Exam') ?></legend>
        <div class="input select">
            <label for="course-id">Course</label>
            <select name="course_id" id="course-id">
                <?php foreach ($courseList as $course) :?>
                    <option value="<?= $course->id ?>"><?= $course->course_title ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <?php
            echo $this->Form->control('title');
            echo $this->Form->control('description');
            echo $this->Form->control('pass_marks');
            echo $this->Form->control('duration');
            echo $this->Form->control('total_marks');
            echo $this->Form->control('test_type');
        ?>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">তারিখ</label>
                <input type="date" name='exam_Date' class="form-control">
            </div>
        </div>
    </fieldset>
    <h3>Add Questions</h3>
    <div id="question-container">
                        <div class="question-div">
                            <h1>Question no </h1>
                            <div class="d-flex flex-wrap pb-3">
                                <div class="form-group input select col-sm-6 col-md-4 d-flex flex-column">

                                    <label for="pwd">Write the Question</label>
                                    <input type="text" class="form-control" required placeholder=""  name="exam_questions[0][question_title]">


                                </div>

                                <div class="form-group col-sm-6 col-md-4">
                                    <label for="pwd">Correct Answer</label>
                                    <select  required="required" id="training-id" class="selectpicker"  name="exam_questions[0][answer]" >
                                        <option value="" >select</option>
                                        <option value="1">Option A</option>
                                        <option value="2"> Option B</option>
                                        <option value="3"> Option C</option>
                                        <option value="4">Option D</option>
                                    </select>
                                </div>
                            </div>
                            <div class="d-flex flex-wrap">
                                <div class="form-group col-sm-6 col-md-4">
                                    <label for="pwd">Answer Option A</label>
                                    <input type="text" class="form-control" required placeholder=""   name="exam_questions[0][option_a]">
                                </div>


                                <div class="form-group col-sm-6 col-md-4">
                                    <label for="pwd">Answer Option B</label>
                                    <input type="text" class="form-control" required placeholder=""   name="exam_questions[0][option_b]">
                                </div>


                                <div class="form-group col-sm-6 col-md-4">
                                    <label for="pwd">Answer Option C</label>
                                    <input type="text" class="form-control" required placeholder=""   name="exam_questions[0][option_c]">
                                </div>


                                <div class="form-group col-sm-6 col-md-4">
                                    <label for="pwd">Answer Option D</label>
                                    <input type="text" class="form-control" required placeholder=""   name="exam_questions[0][option_d]">
                                </div>

                            </div>
                            <div class="d-flex justify-content-center my-2">
                                <label  class="btn btn-danger deleteButton" >Delete Question</label>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="form-group col-sm-6 col-md-6">
                            <label class="btn primary"  id="questionAddButton" >
                                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M10.5619 0.128079C1.96257 1.46516 -2.7429 11.3918 1.70671 18.8083C5.12712 24.5091 12.1259 26.6031 18.0438 23.6962C20.265 22.6051 22.6026 20.2701 23.6949 18.0511C26.6048 12.1396 24.5086 5.1482 18.8017 1.73142C16.4697 0.335409 13.2539 -0.290424 10.5619 0.128079ZM13.1989 5.94357C13.4918 6.16222 13.5666 6.67296 13.614 8.77274L13.6717 11.335L16.2979 11.395C18.8566 11.4535 18.93 11.4685 19.1642 11.9822C19.3423 12.3727 19.3423 12.646 19.1642 13.0366C18.93 13.5503 18.8566 13.5652 16.2979 13.6238L13.6717 13.6838L13.6117 16.3071C13.5531 18.8632 13.5381 18.9364 13.0239 19.1704C12.6329 19.3483 12.3593 19.3483 11.9684 19.1704C11.4541 18.9364 11.4391 18.8632 11.3806 16.3071L11.3205 13.6838L8.69438 13.6238C6.13559 13.5652 6.06227 13.5503 5.828 13.0366C5.64995 12.646 5.64995 12.3727 5.828 11.9822C6.06227 11.4685 6.13559 11.4535 8.69438 11.395L11.3205 11.335L11.3782 8.77274C11.4385 6.10264 11.5924 5.67667 12.4961 5.67667C12.6864 5.67667 13.0027 5.79688 13.1989 5.94357Z" fill="white"/>
                </svg>
                Add Another Question
            </label>

        </div>
    </div>

    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<style>
    .page-content{
        width: 100%;
        display: flex;
        flex-direction: column;
    }
    .page-header{
        display: flex;
        justify-content: space-between;
        margin-bottom: 10px;
        margin-top: 10px;
    }
    .form-content
    {
        display: flex;
        flex-direction: column;
        width: 100%;
    }
    /*.selectpicker{*/

    /*    width: 100%;*/
    /*    height: calc(1.5em + 0.75rem + 2px);*/
    /* }*/
    .selectpicker {
        width: 100%;
        height: calc(1.5em + 0.75rem + 2px);
        background: white;
        border: 1px solid lightgray;

        border-radius: 5px;
    }
    .selectpicker:active{
        border: 3px solid #1585fe;

    }
    .primary{
        background: #0095C8;
        color: white;
    }
    .label-button{
        font-size: 12px;
        padding: 6px;
        background: white;
        color: #007bff;
        border: 1px solid;
        border-radius: 5px;
    }
    .label-button:hover{
        color: white;
        background: #007bff;

    }
    form {
        counter-reset: question;
    }
    h1::after {
        counter-increment: question;
        content:  " " counter(question) ;
    }
</style>

<script>
    let count = 0
    $(function () {

        $('.deleteButton').on("click",  event=> $(event.target).parent().parent().remove())

        let questionDiv = $(".question-div:first-child").prop("outerHTML")
        $('#questionAddButton').click(function(){
            let parentDiv =$('#question-container')
            count++;
            let rowlength = parentDiv.children().length
            let desiredHtml =  questionDiv.replace(/exam_questions\[0\]/g, 'exam_questions['+count+']')
            parentDiv.append(desiredHtml)
            $('.question-div:last-child .deleteButton').on("click", event => $(event.target).parent().parent().remove())
        })
    });




</script>

