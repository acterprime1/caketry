<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Exam[]|\Cake\Collection\CollectionInterface $exams
  */
?>
<div class="exams index large-9 medium-8 columns content">
    <h3><?= __('Exams') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('course_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pass_marks') ?></th>
                <th scope="col"><?= $this->Paginator->sort('duration') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_marks') ?></th>
                <th scope="col"><?= $this->Paginator->sort('test_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('row_status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($exams as $exam): ?>
            <tr>
                <td><?= $this->Number->format($exam->id) ?></td>
                <td><?= $this->Number->format($exam->created_by) ?></td>
                <td><?= $exam->has('course') ? $this->Html->link($exam->course->id, ['controller' => 'Courses', 'action' => 'view', $exam->course->id]) : '' ?></td>
                <td><?= h($exam->title) ?></td>
                <td><?= h($exam->description) ?></td>
                <td><?= $this->Number->format($exam->pass_marks) ?></td>
                <td><?= $this->Number->format($exam->duration) ?></td>
                <td><?= $this->Number->format($exam->total_marks) ?></td>
                <td><?= $this->Number->format($exam->test_type) ?></td>
                <td><?= $this->Number->format($exam->row_status) ?></td>
                <td><?= h($exam->created_at) ?></td>
                <td><?= h($exam->updated_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $exam->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $exam->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $exam->id], ['confirm' => __('Are you sure you want to delete # {0}?', $exam->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
