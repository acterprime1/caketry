<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Course[]|\Cake\Collection\CollectionInterface $courses
  */
?>
<div class="courses index large-9 medium-8 columns content">
    <h3><?= __('Courses') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('course_title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('overall_rating') ?></th>
                <th scope="col"><?= $this->Paginator->sort('test_count') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_published') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('row_status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('course_links') ?></th>
                <th scope="col"><?= $this->Paginator->sort('required_pre_assessment') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pre_assessment_passmark') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pass_mark') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($courses as $course): ?>
            <tr>
                <td><?= $this->Number->format($course->id) ?></td>
                <td><?= h($course->course_title) ?></td>
                <td><?= $this->Number->format($course->overall_rating) ?></td>
                <td><?= $this->Number->format($course->test_count) ?></td>
                <td><?= h($course->is_published) ?></td>
                <td><?= $course->has('user') ? $this->Html->link($course->user->id, ['controller' => 'Users', 'action' => 'view', $course->user->id]) : '' ?></td>
                <td><?= $this->Number->format($course->row_status) ?></td>
                <td><?= h($course->course_links) ?></td>
                <td><?= h($course->required_pre_assessment) ?></td>
                <td><?= $this->Number->format($course->pre_assessment_passmark) ?></td>
                <td><?= $this->Number->format($course->pass_mark) ?></td>
                <td><?= h($course->deleted_at) ?></td>
                <td><?= h($course->created_at) ?></td>
                <td><?= h($course->updated_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $course->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $course->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $course->id], ['confirm' => __('Are you sure you want to delete # {0}?', $course->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
