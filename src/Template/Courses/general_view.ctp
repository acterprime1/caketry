<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Course $course
 */
?>
<div class="courses view large-9 medium-8 columns content">
    <h3><?= h($course->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Course Title') ?></th>
            <td><?= h($course->course_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $course->has('user') ? $this->Html->link($course->user->name, ['controller' => 'Users', 'action' => 'view', $course->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Course Links') ?></th>
            <td><?= h($course->course_links) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($course->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Overall Rating') ?></th>
            <td><?= $this->Number->format($course->overall_rating) ?></td>
        </tr>
       <tr>
            <th scope="row"><?= __('Row Status') ?></th>
            <td><?= $this->Number->format($course->row_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pre Assessment Passmark') ?></th>
            <td><?= $this->Number->format($course->pre_assessment_passmark) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pass Mark') ?></th>
            <td><?= $this->Number->format($course->pass_mark) ?></td>
        </tr>
         <tr>
            <th scope="row"><?= __('Required Pre Assessment') ?></th>
            <td><?= $course->required_pre_assessment ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Course Description') ?></h4>
        <?= $this->Text->autoParagraph(h($course->course_description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Course Description En') ?></h4>
        <?= $this->Text->autoParagraph(h($course->course_description_en)); ?>
    </div>

</div>
