<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Course $course
  */
?>
<div class="courses view large-9 medium-8 columns content">
    <h3><?= h($course->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Course Title') ?></th>
            <td><?= h($course->course_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $course->has('user') ? $this->Html->link($course->user->id, ['controller' => 'Users', 'action' => 'view', $course->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Course Links') ?></th>
            <td><?= h($course->course_links) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($course->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Overall Rating') ?></th>
            <td><?= $this->Number->format($course->overall_rating) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Test Count') ?></th>
            <td><?= $this->Number->format($course->test_count) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Row Status') ?></th>
            <td><?= $this->Number->format($course->row_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pre Assessment Passmark') ?></th>
            <td><?= $this->Number->format($course->pre_assessment_passmark) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pass Mark') ?></th>
            <td><?= $this->Number->format($course->pass_mark) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($course->deleted_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($course->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($course->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Published') ?></th>
            <td><?= $course->is_published ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Required Pre Assessment') ?></th>
            <td><?= $course->required_pre_assessment ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Course Description') ?></h4>
        <?= $this->Text->autoParagraph(h($course->course_description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Course Description En') ?></h4>
        <?= $this->Text->autoParagraph(h($course->course_description_en)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Course Enrolments') ?></h4>
        <?php if (!empty($course->course_enrolments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Course Id') ?></th>
                <th scope="col"><?= __('Enrolment Date') ?></th>
                <th scope="col"><?= __('Row Status') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($course->course_enrolments as $courseEnrolments): ?>
            <tr>
                <td><?= h($courseEnrolments->id) ?></td>
                <td><?= h($courseEnrolments->user_id) ?></td>
                <td><?= h($courseEnrolments->course_id) ?></td>
                <td><?= h($courseEnrolments->enrolment_date) ?></td>
                <td><?= h($courseEnrolments->row_status) ?></td>
                <td><?= h($courseEnrolments->deleted_at) ?></td>
                <td><?= h($courseEnrolments->created_at) ?></td>
                <td><?= h($courseEnrolments->updated_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CourseEnrolments', 'action' => 'view', $courseEnrolments->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CourseEnrolments', 'action' => 'edit', $courseEnrolments->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CourseEnrolments', 'action' => 'delete', $courseEnrolments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $courseEnrolments->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Exams') ?></h4>
        <?php if (!empty($course->exams)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created By') ?></th>
                <th scope="col"><?= __('Course Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Pass Marks') ?></th>
                <th scope="col"><?= __('Duration') ?></th>
                <th scope="col"><?= __('Total Marks') ?></th>
                <th scope="col"><?= __('Test Type') ?></th>
                <th scope="col"><?= __('Row Status') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($course->exams as $exams): ?>
            <tr>
                <td><?= h($exams->id) ?></td>
                <td><?= h($exams->created_by) ?></td>
                <td><?= h($exams->course_id) ?></td>
                <td><?= h($exams->title) ?></td>
                <td><?= h($exams->description) ?></td>
                <td><?= h($exams->pass_marks) ?></td>
                <td><?= h($exams->duration) ?></td>
                <td><?= h($exams->total_marks) ?></td>
                <td><?= h($exams->test_type) ?></td>
                <td><?= h($exams->row_status) ?></td>
                <td><?= h($exams->created_at) ?></td>
                <td><?= h($exams->updated_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Exams', 'action' => 'view', $exams->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Exams', 'action' => 'edit', $exams->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Exams', 'action' => 'delete', $exams->id], ['confirm' => __('Are you sure you want to delete # {0}?', $exams->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
