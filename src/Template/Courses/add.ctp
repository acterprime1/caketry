<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="courses form large-9 medium-8 columns content">
    <?= $this->Form->create($course) ?>
    <fieldset>
        <legend><?= __('Add Course') ?></legend>
        <?php
            echo $this->Form->control('course_title');
            echo $this->Form->control('course_description');
            echo $this->Form->control('course_description_en');
            echo $this->Form->control('course_links');
            echo $this->Form->control('pass_mark');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
