<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="userDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($userDetail) ?>
    <fieldset>
        <legend><?= __('Add User Detail') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('name');
            echo $this->Form->control('phone');

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
