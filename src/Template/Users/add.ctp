<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
            echo $this->Form->control('role_id', ['options' => $roles, 'empty' => false]);
            echo $this->Form->control('user_name');
            echo $this->Form->control('user_detail.name');
            echo $this->Form->control('email');
            echo $this->Form->control('user_detail.phone');
            echo $this->Form->control('password');

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
