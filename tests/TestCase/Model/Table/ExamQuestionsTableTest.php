<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExamQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExamQuestionsTable Test Case
 */
class ExamQuestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ExamQuestionsTable
     */
    public $ExamQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.exam_questions',
        'app.exams',
        'app.courses',
        'app.users',
        'app.roles',
        'app.user_details',
        'app.course_enrolments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ExamQuestions') ? [] : ['className' => ExamQuestionsTable::class];
        $this->ExamQuestions = TableRegistry::get('ExamQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ExamQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
