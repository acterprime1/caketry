<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CourseEnrolmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CourseEnrolmentsTable Test Case
 */
class CourseEnrolmentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CourseEnrolmentsTable
     */
    public $CourseEnrolments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.course_enrolments',
        'app.users',
        'app.roles',
        'app.user_details',
        'app.courses',
        'app.exams',
        'app.exam_questions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CourseEnrolments') ? [] : ['className' => CourseEnrolmentsTable::class];
        $this->CourseEnrolments = TableRegistry::get('CourseEnrolments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CourseEnrolments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
